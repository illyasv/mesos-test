# Import all plugins from `rel/plugins`
# They can then be used by adding `plugin MyPlugin` to
# either an environment, or release definition, where
# `MyPlugin` is the name of the plugin module.
Path.join(["rel", "plugins", "*.exs"])
|> Path.wildcard()
|> Enum.map(&Code.eval_file(&1))

use Mix.Releases.Config,
    # This sets the default release built by `mix release`
    default_release: :default,
    # This sets the default environment used by `mix release`
    default_environment: Mix.env()

# For a full list of config options for both releases
# and environments, visit https://hexdocs.pm/distillery/configuration.html


# You may define one or more environments in this file,
# an environment's settings will override those of a release
# when building in that environment, this combination of release
# and environment configuration is called a profile

environment :dev do
  # If you are running Phoenix, you should make sure that
  # server: true is set and the code reloader is disabled,
  # even in dev mode.
  # It is recommended that you build with MIX_ENV=prod and pass
  # the --env flag to Distillery explicitly if you want to use
  # dev mode.
  set dev_mode: true
  set include_erts: false
  set cookie: :"M_u2(@Cez<OZ8k>6)l|~UQOx]sNAVG8Y)JKz!m2bf.KuVaR4p:^Uv/]S2vUHxRk9"
end

environment :prod do
  plugin Messos.PhoenixDigestTask
  set include_erts: true
  set include_src: false
  set cookie: :"9~2{Vm,x,x?sV(*J|aa|/9%i7izqKVH_gK@wYr4`r]Q]r<a[%hYO%1t4LtdgixF)"
end

# You may define one or more releases in this file.
# If you have not set a default release, or selected one
# when running `mix release`, the first release in the file
# will be used by default

release :messos do
  set version: current_version(:messos)
  set applications: [
    :runtime_tools
  ]
end

