defmodule MessosWeb.PageController do
  use MessosWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
